# FCC-Tribute-Page-Project
Tribute Page Project for FreeCodeCamp

This page was build with HTML, CSS, and Javascript.

The site can be viewed at <a href="https://larrytooley.github.io/fcc-build-a-tribute-page/" target="_blank">https://larrytooley.github.io/fcc-build-a-tribute-page/</a>